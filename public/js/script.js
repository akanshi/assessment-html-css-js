let prodData = []
let saveData = []

//This function will fetch products from db.json

function getProducts(){
   return fetch("http://localhost:3000/products",{method:"GET"})
   .then((res)=>{
      if(res.ok){
        return res.json()
      }
      else{
        return Promise.reject(res.status);
      }
   })
   .then((data)=>{
      prodData = data;
      displayProducts(prodData);
      return data;
   })
   .catch((err)=>{
    console.log(err)
   })
}

//This fn will fetch save for later from db.json

function getSaveForLater(){
    return fetch("http://localhost:3000/saveforLater",{method:"GET"})
    .then((res)=>{
        if(res.ok){
          return res.json();
        }
        else{
          return Promise.reject(res.status);
        }
     })
     .then((data)=>{
        saveData = data;
        displaySaveLater(saveData);
        return data;
     })
   .catch((err)=>{
    console.log(err)
   })
}

//This fn will display products on html page

let displayProducts = (productArr) =>{
    let productId = document.getElementById("products");
    let htmlString = ""
    
    productArr.forEach(element => {
        htmlString += `
                       <div class="prod card" style="width: 18rem;">
                       <li><img src='${element.thumbnail}' class='card-img-top'></li>
                       <div class='card-body'>
                       <li><h2 class='card-title'>${element.title}</h2></li>
                       <li><div>${element.description}</div></li>
                       <li><div class="decor">PRICE: ${element.price}</div></li>
                       <li><div class="decor">RATING: ${element.rating}</div></li>
                       <li><div class="decor">STOCK: ${element.stock}</div></li>
                       <li><div class="decor">CATEGORY: ${element.category}</div></li>
                       <li><button class="btn btn-success" onclick="addToSaveLater(${element.id})">Add to saveForLater</button></li>
                       </div>
                       </div>
                      `
    })

    productId.innerHTML = htmlString;
}

//This fn will display saved items on html page

let displaySaveLater = (saveLaterArr) =>{
    let productId = document.getElementById("saveForLater");
    let htmlString = ""

    saveLaterArr.forEach(element => {
        htmlString += 
        `
        <div class="prod card" style="width: 18rem;">
        <li><img src='${element.thumbnail}' class='card-img-top'></li>
        <div class='card-body'>
        <li><h2 class='card-title'>${element.title}</h2></li>
        <li><div>${element.description}</div></li>
        <li><div class="decor">PRICE: ${element.price}</div></li>
        <li><div class="decor">RATING: ${element.rating}</div></li>
        <li><div class="decor">STOCK: ${element.stock}</div></li>
        <li><div class="decor">CATEGORY: ${element.category}</div></li>
        <li><button class="btn btn-danger" onclick="deleteItem(${element.id})">DeleteItem</button></li>
        </div>
        </div>
        `
    })

    productId.innerHTML = htmlString;
}


//This fn will add all the items in save later

function addToSaveLater(id){
   let product = prodData.find(item=>{
     if(item.id == id){
        return item;
     }
   })

   let savedItem = saveData.find(item=>{
      if(item.id == id){
        return item;
      }
   })

   if(savedItem){
     alert("This item is already in saveForLater")
     return Promise.reject(new Error("Item is already in saveForLater"));
   }
   else{
     return fetch('http://localhost:3000/saveforLater',{
        method : 'POST',
        headers : {'content-type':'application/json'},
        body: JSON.stringify(product)
     })
     .then((res)=>{
        if(res.ok){
            return res.json()
        }
     })
     .then((data)=>{
        saveData.push(data);
        displaySaveLater(saveData);
        return data;
     })
   }
    
}

//This fn will delete items from saveforLater
function deleteItem(id){
    return fetch(`http://localhost:3000/saveforLater/${id}`,{method:'DELETE'})
    .then((res) => {
        if(res.ok){
            return res.json()
        }
    })
    .then((data) => {
        saveData.splice(id);
        displaySaveLater(saveData);
        return data;
    })
}

getProducts();
getSaveForLater();